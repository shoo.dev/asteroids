﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace App.Code.Settings.Scriptables
{
    [CreateAssetMenu(fileName = "Game Input Preset", menuName = "Game Input Preset", order = 0)]
    public class GameInputPreset : ScriptableObject
    {
        [field: SerializeField] public InputAction Rotate { get; private set; }
        [field: SerializeField] public InputAction Thrust { get; private set; }
        [field: SerializeField] public InputAction Bullet { get; private set; }
        [field: SerializeField] public InputAction Laser { get; private set; }
        [field: SerializeField] public InputAction Start { get; private set; }
        [field: SerializeField] public InputAction Finish { get; private set; }

        private void OnEnable()
        {
            Rotate.Enable();
            Thrust.Enable();
            Bullet.Enable();
            Laser.Enable();
            Start.Enable();
            Finish.Enable();
        }

        private void OnDisable()
        {
            Rotate.Disable();
            Thrust.Disable();
            Bullet.Disable();
            Laser.Disable();
            Start.Disable();
            Finish.Disable();
        }
    }
}