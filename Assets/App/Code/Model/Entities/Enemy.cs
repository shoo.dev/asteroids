﻿using App.Code.Model.Entities.Base;
using UnityEngine;

namespace App.Code.Model.Entities
{
    public class Enemy : Body
    {
        private readonly float _speed;
        
        public Enemy(Vector2 position, float speed, float radius) : 
            base(position, Vector2.zero, radius)
        {
            _speed = speed;
        }

        public void SetMovementTowards(Vector2 position)
        {
            Movement = (position - Position).normalized * _speed;
        }
    }
}