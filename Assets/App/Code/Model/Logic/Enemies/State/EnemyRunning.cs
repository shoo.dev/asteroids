﻿using App.Code.Model.Entities;

namespace App.Code.Model.Logic.Enemies.State
{
    public sealed class EnemyRunning : EnemyExistsState
    {
        private readonly float _reload;
        private float _current;

        public EnemyRunning(Enemy enemy, float reload) : base(enemy)
        {
            _reload = reload;
        }

        public bool TryShoot(float deltaTime)
        {
            if ((_current -= deltaTime) < 0f)
            {
                _current = _reload;
                return true;
            }

            return false;
        }
    }
}