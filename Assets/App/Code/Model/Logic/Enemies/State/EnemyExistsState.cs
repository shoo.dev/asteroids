﻿using App.Code.Model.Entities;

namespace App.Code.Model.Logic.Enemies.State
{
    public abstract class EnemyExistsState : EnemyState
    {
        public Enemy Enemy { get; }

        protected EnemyExistsState(Enemy enemy)
        {
            Enemy = enemy;
        }
    }
}