﻿using System;
using App.Code.Model.Entities;
using App.Code.Model.Interfaces.Base;
using App.Code.Model.Logic.Bullets;
using App.Code.Model.Logic.Enemies.State;
using App.Code.Model.Logical.Field;
using App.Code.Settings;
using UnityEngine;

namespace App.Code.Model.Logic.Enemies
{
    public class EnemyModel : ISource<Enemy>, IPointable
    {
        public event Action<Enemy> Create;
        public event Action<Enemy> Remove;

        public event Action Point;

        private readonly GameField _field;
        private readonly EnemyBulletModel _bullets;
        private readonly SpaceshipModel _spaceship;
        private readonly EnemySettings _settings;

        private EnemyState _state;

        public EnemyModel(GameField field, EnemyBulletModel bullets, SpaceshipModel spaceship, EnemySettings settings)
        {
            _field = field;
            _bullets = bullets;
            _spaceship = spaceship;
            _settings = settings;
            _state = new EnemyWaiting(settings.Spawn);
        }

        public bool ApplyPlayerBullet(Vector2 position)
        {
            if (_state is EnemyExistsState state 
                && state.Enemy.HasIntersectionWithPoint(position))
            {
                Kill(state);
                return true;
            }

            return false;
        }

        public void ApplyLaser(Ray2D ray)
        {
            if (_state is EnemyExistsState state 
                && state.Enemy.HasIntersectionWithRay(ray))
            {
                Kill(state);
            }
        }

        public void Bind() => _spaceship.Remove += OnSpaceshipRemove;

        public void Drop() => _spaceship.Remove -= OnSpaceshipRemove;

        private void OnSpaceshipRemove(Spaceship _)
        {
            _state = _state switch
            {
                EnemyExistsState s => 
                    new EnemyWon(s.Enemy),
                EnemyWaiting => 
                    new EnemyWonAndDied(),
                _ => 
                    throw new ArgumentOutOfRangeException()
            };
        }

        private void Kill(EnemyExistsState state)
        {
            var enemy = state.Enemy;
            Remove?.Invoke(enemy);
            Point?.Invoke();

            _state = state switch
            {
                EnemyRunning => 
                    new EnemyWaiting(_settings.Spawn),
                EnemyWon => 
                    new EnemyWonAndDied(),
                _ => throw new ArgumentOutOfRangeException(nameof(state), state, "Unknown state!")
            };
        }

        private void UpdateWaiting(EnemyWaiting waiting, float deltaTime)
        {
            if (waiting.IsDone(deltaTime))
            {
                var enemy = new Enemy(_field.GetRandomPositionOnBorder(), _settings.Speed, _settings.Radius);
                Create?.Invoke(enemy);
                _state = new EnemyRunning(enemy, _settings.Bullet.Reload);
            }
        }

        private void UpdateRunning(EnemyRunning running, float deltaTime)
        {
            var enemy = running.Enemy;
            
            if (_spaceship.TryGetPosition(out var position))
            {
                enemy.SetMovementTowards(position);
                enemy.ApplyMovement(deltaTime, _field);
            }
            
            if (_spaceship.ApplyBody(enemy.Position, enemy.Radius))
            {
                Remove?.Invoke(enemy);
                return;
            }

            if (running.TryShoot(deltaTime))
            {
                var direction = (position - enemy.Position).normalized;
                _bullets.Add(new Bullet(
                    enemy.Position + direction * enemy.Radius, 
                    direction * _settings.Bullet.Speed, 
                    _settings.Bullet.Lifetime));
            }
        }

        public void Update(float deltaTime)
        {
            _bullets.Update(deltaTime);

            switch (_state)
            {
                case EnemyWaiting waiting:
                    UpdateWaiting(waiting, deltaTime);
                    break;
                case EnemyRunning running:
                    UpdateRunning(running, deltaTime);
                    break;
                case EnemyWon winner:
                    winner.Enemy.ApplyMovement(deltaTime, _field);
                    break;
            }
        }
    }
}