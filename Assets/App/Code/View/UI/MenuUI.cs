﻿using TMPro;
using UnityEngine;

namespace App.Code.View.UI
{
    public class MenuUI : MonoBehaviour
    {
        [SerializeField] private TMP_Text _textStart;
        [SerializeField] private TMP_Text _textControl;
        
        public void Refresh(string start, string bullet, string laser)
        {
            _textStart.text = $"Press {start} \n to start";
            _textControl.text = $"{bullet} - bullet \n {laser} - laser";
        }
    }
}